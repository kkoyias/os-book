import java.net.*;
import java.io.*;

public class DateClient{

	static final int PORT = 6013;
	public static void main(String[] args)  {
		try {
			// this could be changed to an IP name or address other than the localhost
			Socket sock = new Socket("127.0.0.1", PORT);
			InputStream in = sock.getInputStream();
			BufferedReader bin = new BufferedReader(new InputStreamReader(in));

			String line;
			while( (line = bin.readLine()) != null)
				System.out.println("\u001B[1m >> client\u001B[0m: " + line);
				
			sock.close();
		}
		catch (IOException ioe) {System.err.println(ioe);}
	}
}
