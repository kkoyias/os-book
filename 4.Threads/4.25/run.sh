#!/bin/bash

exe(){
    cd $build
    java $@
}

# config
build=$PWD/build
server=DateServer
client=DateClient
max=3
secs=3

# go
echo -n "Compiling..."
make > /dev/null
echo "done"

echo "Running server on the background..."
cd $build
exe $server &

echo "Running $(expr $max + 1) clients every $secs seconds..."
for ((i = 0; i < $max + 1; i++))
do
    sleep $secs
    exe $client
done
