import java.net.*;
import java.io.*;

class Worker implements Runnable{

	Socket client;
	public Worker(Socket client){this.client = client;}

	public void run(){

		try{
			PrintWriter pout = new PrintWriter(client.getOutputStream(), true);
			// write the Date to the socket
			pout.println(new java.util.Date().toString());
	
			// close the socket and resume listening for more connections
			client.close();
		}
		catch(IOException ioe){ System.err.println(ioe); }

		System.out.println("\t\u001B[1m >> worker\u001B[0m: client " + client.getInetAddress() + " served");
	}
}

public class DateServer{

	static final int PORT = 6013, MAX_CLIENTS = 3;
	static void serverMessage(String msg){ System.out.print("\u001B[1m >> server\u001B[0m: " + msg);}
	static ServerSocket sock;

	public static void main(String[] args)  {
		try {
			Thread worker;
			ServerSocket sock = new ServerSocket(PORT);
			serverMessage("running on port " + PORT + "...\n");

			// now listen for connections
			for(int i = 0; i < MAX_CLIENTS; i++) {

				// listening for connections
				worker = new Thread(new Worker(sock.accept()));
				worker.start();
				serverMessage("established connection\n");
			}

			sock.close();
			serverMessage("reached maximum connections(" + MAX_CLIENTS + ")\n");
			serverMessage("Bye");
		}
		catch (IOException ioe) {
				System.err.println(ioe);
		}
	}
}
