#!/bin/bash

input=input.tmp
src=fib.c
cc=gcc
num=10
lflags=-lpthread

echo $num > $input
$cc $src $lflags
./a.out < $input
rm $input