#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *routine(void *);
unsigned int *seq;
int main(){
    unsigned int sz;
    pthread_t id;

    fprintf(stdout, "Please specify the size of the fibonacci sequence: ");
    fscanf(stdin, "%u", &sz);

    if((seq = malloc((sz + 1) * sizeof(unsigned int))) == NULL)
        return 1;
    seq[0] = sz;

    routine(NULL);
    //pthread_create(&id, NULL, routine, NULL);
    //pthread_join(id, NULL);

    fputc('<', stdout);
    for(int i = 0; i < sz - 1; i++) 
        fprintf(stdout, "%u, ", seq[i + 1]);
    fprintf(stdout, "%u>\n", seq[sz]);

    free(seq);
    return 0;
}

void *routine(void *arg){
    if(seq == NULL || seq[0] < 2)
        return NULL;

    seq[1] = 0;
    seq[2] = 1;

    for(int i = 0; i < seq[0] - 2; i++)
        seq[i + 3] = seq[i + 1] + seq[i + 2];

    return NULL;
}