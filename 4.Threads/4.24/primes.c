#include <stdio.h>
#include <assert.h>
#include <pthread.h>

void *routine(void *);
int is_prime(int);

int main(){
    unsigned int num;
    pthread_t id;

    fprintf(stdout, "Please enter a positive integer\n");
    fscanf(stdin, "%u", &num);

    pthread_create(&id, NULL, routine, &num);
    pthread_join(id, NULL);
    return 0;
}

void *routine(void *num){
    assert(num);

    for(int i = 2; i < *((int*)num); i++){

        if(is_prime(i))
            fprintf(stdout, "%u\n", i);
    }

    return NULL;
}

int is_prime(int num){
    int j;
    if(num < 4)
        return 1;

    for(j = 2; j * j < num && (num % j); j++);
    return num % j != 0;
}