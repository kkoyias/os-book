#ifndef _API_H
#define _API_H

    #define NUMS 10
    #define STATS 3

    void *set_max(void *);
    void *set_min(void *);
    void *set_avg(void *);

#endif