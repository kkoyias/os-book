#include <stdint.h>
#include <assert.h>
#include <stdlib.h>
#include "api.h"

void set_best(int *, int *, int, int (*)(int, int));
int max_cmp(int a, int b){return ((a) > (b) ? (a) : (b));} // max of 2
int min_cmp(int a, int b){return ((a) > (b) ? (b) : (a));} // min of 2

void *set_max(void *arr){ extern int max; set_best((int*)arr, &max, INT32_MIN, max_cmp); return NULL;}
void *set_min(void *arr){ extern int min; set_best((int*)arr, &min, INT32_MAX, min_cmp); return NULL;}

void set_best(int *arr, int *bst, int val, int (*cmp)(int, int)){
    assert(arr && bst && cmp);

    *bst = val; // initial value of best
    for(int i = 1; i < arr[0]; i++)
        *bst = cmp(*bst, arr[i]);   // update if arr[i] is even better
}

void *set_avg(void *arr){
    extern float avg;
    assert(arr);

    avg = 0;
    for(int i = 1; i < ((int*)arr)[0]; i++)
        avg += ((int*)arr)[i];
    avg /= ((int*)arr)[0];
    return NULL;
}