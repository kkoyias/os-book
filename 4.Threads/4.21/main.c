#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include "api.h"

int max, min;
float avg;

int main(){
    int arr[NUMS + 1] = {NUMS};
    void *(*stats[STATS])(void *) = {set_max, set_min, set_avg}; // statistic extractors
    pthread_t ids[STATS];

    // create array with random numbers
    srand(time(NULL));
    for(int i = 0; i < NUMS; i++)
        fprintf(stdout, "%d ", arr[i + 1] = rand() % INT8_MAX);
    
    // kick-start threads
    for(int i = 0; i < STATS; i++)
        pthread_create(ids + i, NULL, stats[i], arr);

    // wait for them to finish
    for(int i = 0; i < STATS; i++)
        pthread_join(ids[i], NULL);

    fprintf(stdout, "\n\e[1;4mStats\n\e[0m * max: %d\n * min: %d\n * avg: %.2f\n", max, min, avg);
    return 0;
}