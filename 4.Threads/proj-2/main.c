#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "api.h"

#define SIZE 40
#define MAX 100
#define RANGE(a, b) (a) + ((b) << 16)
#define FAIL "31mFailure"
#define SUCC "32mSuccess"

// globals for the workers to share
int list[SIZE + 1];
int sorted_list[SIZE + 1];
pthread_mutex_t mutex;
pthread_cond_t cond;
int done = 0;

int main(){

    int left, right;
    pthread_t left_thread, right_thread, merge_thread;

    // create a list with random numbers
    list_init(list, SIZE);

    // copy it back to another and sort it
    memcpy(sorted_list, list, sizeof(int) * (SIZE + 1));
    qsort(sorted_list + 1, list[0], sizeof(int), __list_cmp);
    
    // kick-start threads
    left = RANGE(0, SIZE/2 - 1);
    pthread_create(&left_thread, NULL, sorter, &left);

    right = RANGE(SIZE/2, SIZE - 1);
    pthread_create(&right_thread, NULL, sorter, &right);
    pthread_create(&merge_thread, NULL, merger, NULL);

    // wait for the last one(merging thread) to finish
    pthread_join(merge_thread, NULL);

    // display result
    list_print(list);
    list_print(sorted_list);
    fprintf(stdout, "\e[1;%s\e[0m\n", memcmp(list + 1, sorted_list + 1, sizeof(int) * list[0]) ? FAIL : SUCC);

    return 0;
}