#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include "api.h"

// initialize an array of integers with random numbers
// storing the size at index 0
void list_init(int *list, int sz){
    assert(list);
    list[0] = sz;

    srand(time(NULL));
    for(int i = 0; i < sz; i++)
        list[i + 1] = rand() % MAX_NUM;
}

// display an array of integers holding it's size at the very 1st cell
void list_print(int *list){
    assert(list);

    fputc('[', stdout);
    for(int i = 0; i < list[0] - 1; i++)
        fprintf(stdout, "%d, ", list[i + 1]);
    fprintf(stdout, "%d]\n", list[list[0]]);
}

// an integer comparator matching stdlib.qsort comparator prototype
int __list_cmp(const void *a, const void *b){return *((int*)a) - *((int*)b); }