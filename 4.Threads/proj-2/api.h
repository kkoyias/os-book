#ifndef _API_H
#define _API_H

    #define MAX_NUM 100
    #define NOT_DONE 0
    #define DONE 1

    // list methods
    void list_init(int *, int);
    void list_print(int *);
    int __list_cmp(const void *, const void *);


    // thread methods
    void *sorter(void *);
    void *merger(void *);


#endif