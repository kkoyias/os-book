#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include "api.h"

// internal implementation utility function declarations
int merge(int *);

// sort the requested half of a global list
void *sorter(void *arg){
    extern int list[], done;
    extern pthread_mutex_t mutex;
    extern pthread_cond_t cond;
    short *range;
    assert(arg);

    // extract the range out of the argument passed
    range = (short*)arg;
    fprintf(stdout, "sorter %ld: range [%d, %d]...", pthread_self(), range[0], range[1]);

    // sort this part of the list using stdlib.qsort(no need to lock because ranges do not overlap)
    qsort(list + 1 + range[0], range[1] - range[0] + 1, sizeof(int), __list_cmp);

    // inform that this side got sorted
    pthread_mutex_lock(&mutex);
    fprintf(stdout, "done(sorter %ld)\n", pthread_self());
    done++;
    pthread_mutex_unlock(&mutex);
    pthread_cond_signal(&cond);


    return NULL;
}

void *merger(void *arg){
    extern int list[], done;
    extern pthread_mutex_t mutex;
    extern pthread_cond_t cond;

    // while the two halves are still being sorted, wait
    pthread_mutex_lock(&mutex);
    while(done < 2)
        pthread_cond_wait(&cond, &mutex);
    pthread_mutex_unlock(&mutex);

    list_print(list);
    merge(list);
    return NULL;
}

// merge a list the size of which is stored in the 1st cell
// and the left and right half are both sorted already
int merge(int *list){
    int *tmp, i, j, sz, count;
    assert(list);

    // create a temporary array to merge in the halves
    if((tmp = malloc(sizeof(int) * (sz = list[0]))) == NULL)
        return -1;

    // merge the two halves
    for(i = count = 0, j = sz/2, list++; i < sz/2 && j < sz; count++)
        tmp[count] = __list_cmp(list + i, list +  j) < 0 ? list[i++] : list[j++];

    // put in the remaindings as well
    if(j < sz)
        memcpy(tmp + count, list + j, sizeof(int) * (sz - j));
    else 
        memcpy(tmp + count, list + i, sizeof(int) * (sz/2 - i));

    // copy back to the initial list
    memcpy(list, tmp, sizeof(int) * sz);
    free(tmp);
    return 0;
}