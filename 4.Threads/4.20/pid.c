#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "bitmap.h"

#define THREADS 15
#define MAX_SECS 10

void *request_pid(void *);
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int main(){
    pthread_t ids[THREADS];

    for(int i = 0; i < THREADS; i++)
        pthread_create(ids + i, NULL, request_pid, NULL);

    for(int i = 0; i < THREADS; i++)
        pthread_join(ids[i], NULL);

    return 0;
}

void *request_pid(void *arg){
    int pid, secs;

    // get a lock before asking for a pid to avoid 
    // having 2 threads asking almost simultaneously
    pthread_mutex_lock(&mutex);
    fprintf(stdout, "%ld: \e[32mobtained\e[0m %d, I'll be busy for %d sec\n", 
            pthread_self(), pid = allocate_pid(), secs = pthread_self() % MAX_SECS);
    pthread_mutex_unlock(&mutex);
    sleep(secs);


    pthread_mutex_lock(&mutex);
    fprintf(stdout, "%ld: \e[31mreleasing\e[0m %d -- %s\e[0m\n", pthread_self(),
        pid, release_pid(pid) ? "\e[31mfail" : "\e[32mok" );
    pthread_mutex_unlock(&mutex);

    return NULL;
}