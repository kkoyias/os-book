#!/bin/bash

# configurations
input=input.tmp.txt
src=src.tmp.txt
dst=dst.tmp.txt
c_file=file_copy.c
exe=$(echo $c_file | cut -d '.' -f1)

# create random source file
echo -n "Creating random source file $src..."
for i in {1..5}
do
    fortune >> $src
done
echo "done"

# display it
cat $src

# create input for the program to be run
echo $src > $input
echo $dst >> $input

# compile and run program with the input created
gcc -o $exe $c_file
./$exe < $input > /dev/null

# test if everything worked out as expected
diff -s $src $dst > /dev/null
if [ $? -ne 0 ]
then
    echo -e "\e[1;31mFail\e[0m: files differ"
else 
    echo -e "\e[1;32mSuccess\e[0m: files are identical"
fi

# clean up and exit
rm $src $dst $input $exe
exit 0