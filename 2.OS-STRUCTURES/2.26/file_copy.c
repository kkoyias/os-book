/*
 * copy a file to another getting their paths from stdin, using high-level IO
 * compile with gcc and test or run the testing bash script provided
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX_PATH 4096 // largest possible file path

void get_path(const char *, char *);

int main(){
    char src[MAX_PATH], dst[MAX_PATH], ch;
    FILE *fs, *fd;

    // get path for source and destination files
    get_path("Please provide source file path\n", src);
    get_path("Please provide destination file path\n", dst);

    // open files
    if((fs = fopen(src, "r")) == NULL || (fd = fopen(dst, "w")) == NULL){
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    // copy src to dst char-by-char
    while((ch = fgetc(fs)) != EOF)
        fputc(ch, fd);

    // let go of resources and exit
    fclose(fs);
    fclose(fd);
    return 0;
}

void get_path(const char *msg, char *buf){
    fprintf(stdout, "%s", msg);
    fscanf(stdin, "%s", buf);
}