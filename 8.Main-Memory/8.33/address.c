#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>

void raise_error(char *, ...);
int exp_of(int);

int main(int argc, char *argv[]){
    unsigned int addr, page_bits, mask;

    // verify correct usage
    if(argc < 2 || ((addr = atoi(argv[1])) == 0) && strcmp(argv[1], "0") != 0)
        raise_error("Usage: %s <logical address>\n", argv[0]);

    // find number of bits need for the offset within a page
    page_bits = exp_of(getpagesize());

    // a mask for the lower $page_bits bits
    mask = (1 << page_bits) - 1;

    // split address in a high(page number) and a low(offset) part
    fprintf(stdout, "The address %d contains\npage number = %d\noffset = %d\n", 
        addr, addr >> page_bits, addr & ((1 << page_bits) - 1));

    return 0;
}

int exp_of(int num){
    int bits;

    for(bits = 0; num > 0; bits++)
        num >>= 1;
    return bits - 1;
}

// display a message and exit with code 1
void raise_error(char *format, ...){
    va_list arg;

    va_start(arg, format);
    vfprintf(stdout, format, arg);
    va_end(arg);
    exit(EXIT_FAILURE);
}