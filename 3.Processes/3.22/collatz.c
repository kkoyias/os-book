#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <assert.h>

#define NAME "shmem"
#define MODE 0666
#define SIZE 31 // size of shm is fixed(at most 30 integers sequences)

int collatz(unsigned int, const char *, int, mode_t, size_t, int);
void *shm_get(const char *, int, mode_t, size_t, int);

int main(int argc, char *argv[]){
    pid_t pid;
    int number, *shm, status;

    // get starting number from command line
    if(argc < 2 || (number = atoi(argv[1])) == 0){
        fprintf(stdout, "Usage: %s <number>\n", argv[0]);
        return 1;
    }

    // create shared memory region for the parent to read from
    if((shm = shm_get(NAME, O_CREAT | O_RDWR, MODE, SIZE * sizeof(int), PROT_READ)) == NULL){
        perror("-- shared memory creation --");
        return 2;
    }

    // create child process
    if((pid = fork()) < 0){
        perror("-- fork --");
        return 3;
    }

    // child
    else if(pid == 0)
        return collatz(number, NAME, O_RDWR, MODE, SIZE * sizeof(int), PROT_WRITE) != 0 ? 1 : 0;

    // parent
    wait(&status);
    fprintf(stdout, " >> \e[1mParent\e[0m: child exited with status %hhu stored a sequence of size %d,"
                    " shared memory now looks like this\n[", WEXITSTATUS(status), shm[0]);
    for(int i = 0; i < shm[0] - 1; i++)
        fprintf(stdout, "%u, ", shm[i + 1]);
    fprintf(stdout, "%u]\n", shm[shm[0]]);
    return 0;
}

// place collatz conjecture into the shared memory region
int collatz(unsigned int number, const char *name, int oflag, mode_t mode, size_t size, int prot){
    int fd, *shm, i;
    assert(name);

    // open and map shared memory region given its name
    if((fd = shm_open(name, oflag, mode)) < 0 ||(shm = mmap(0, size, prot, MAP_SHARED, fd, 0)) == MAP_FAILED)
        return 1;
    
    fprintf(stdout, " >> \e[1mChild\e[0m: placing collatz conjecture starting"
                    " from %u into the shared memory region...", number);
    for(i = 0; number > 1 && i < size/sizeof(int) - 1; i++){
        shm[i + 1] = number;
        number = number % 2 ? 3 * number + 1 : number/2;
    }
    shm[0] = i; // numbers written
    fprintf(stdout, "done\n");

    return shm_unlink(name);
}

// get a shared memory segment and map it into process's virtual address space
void *shm_get(const char *name, int oflag, mode_t mode, size_t size, int prot){
    int fd;
    void *rv;
    assert(name);

    // create shared memory segment and set size
    if((fd = shm_open(name, oflag, mode)) < 0 || ftruncate(fd, size) < 0)
        return NULL;

    // map region right into the processe's virtual memory
    return (rv = mmap(0, size, prot, MAP_SHARED, fd, 0)) == MAP_FAILED ? NULL : rv;
}