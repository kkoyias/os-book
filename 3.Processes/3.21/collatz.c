#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

void collatz(unsigned int);
int main(int argc, char *argv[]){
    pid_t pid;
    unsigned int number;

    if(argc < 2 || (number = atoi(argv[1])) == 0){
        fprintf(stdout, "Usage: %s <number>\n", argv[0]);
        return 1;
    }

    if((pid = fork()) < 0){
        perror("-- fork --");
        return 2;
    }

    // child
    else if(pid == 0){
        collatz(number);
        return 0;
    }

    // parent
    wait(NULL);
    fprintf(stdout, " >> \e[1mParent\e[0m: all done\n");
    return 0;
}

void collatz(unsigned int number){

    fprintf(stdout, " >> \e[1mChild\e[0m: collatz conjecture starting from %u\n\t<", number);
    while(number > 1){
        fprintf(stdout, "%u ", number);
        number = number % 2 ? 3 * number + 1 : number/2;
    }
    fprintf(stdout, "%u>\n", number);
}