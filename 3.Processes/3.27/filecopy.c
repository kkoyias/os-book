/*
 * copy a file to another using low-level IO, chunks of fixed-size bytes
 * use ./test.sh to verify the result with random input (fortune must be installed)
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define RD_END 0
#define WR_END 1
#define BUFF_SZ 256
#define MODE 0666
int main(int argc, char *argv[]){
    pid_t pid;
    size_t bytes;
    char buff[BUFF_SZ];
    int src, dst, fd[2];

    if(argc < 3){
        fprintf(stderr, "Usage: %s <src_file> <dst_file>\n", argv[0]);
        return 1;
    }

    // create common pipe
    if(pipe(fd) < 0){
        perror("-- pipe --");
        return 2;
    }

    // create child process
    if((pid = fork()) < 0){
        perror("-- fork --");
        return 3;
    }

    // -- parent --
    else if(pid > 0){
        close(fd[RD_END]);
        if((src = open(argv[1], O_RDONLY)) < 0){
            perror("-- open source --");
            return 4;
        }

        // read from source and put into pipe
        while((bytes = read(src, buff, BUFF_SZ)) > 0)
            write(fd[WR_END], buff, bytes);

        // close fds not needed
        close(src);
        close(fd[WR_END]);
        return 0;
    }

    // -- child --
    else if((dst = open(argv[2], O_CREAT | O_WRONLY, MODE)) < 0){
        perror("-- open destination --");
        return 4;
    }
    close(fd[WR_END]);
        
    // read from pipe and put into destination
    while((bytes = read(fd[RD_END], buff, BUFF_SZ)) > 0)
        write(dst, buff, bytes);

    // close fds not needed
    close(fd[RD_END]);
    close(dst);
    return 0;
}