#!/bin/bash

# configurations
src=src.tmp.txt
dst=dst.tmp.txt
c_file=filecopy.c
exe=$(echo $c_file | cut -d '.' -f1)

# make sure fortune is installed
which fortune >& /dev/null
if [ $? -ne 0 ]
then
    echo " >> Installing fortune is sudo mode"
    sudo apt install fortune
fi

# create random source file
echo -n " >> Creating random source file $src..."
for i in {1..3}
do
    fortune >> $src
done
echo "done"

# display it
cat $src

# compile and run program with the input created
gcc -o $exe $c_file
./$exe $src $dst $> /dev/null

# test if everything worked out as expected
diff -s $src $dst > /dev/null
if [ $? -ne 0 ]
then
    echo -e " >> \e[1;31mFail\e[0m: files differ"
else 
    echo -e " >> \e[1;32mSuccess\e[0m: files are identical"
fi

# clean up and exit
rm $src $dst $input $exe
exit 0