#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "bitmap.h"

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define ARGS 2
#define CREATE 9 // default # of processes created
#define TERM 3  // default # of processes to terminate

int main(int argc, char *argv[]){

    int create = CREATE, term = TERM, *props[ARGS] = {&create, &term};
    char *flags[ARGS] = {"--create", "--terminate"};

    // handle command line arguments, if any, or else use default values
    for(int i = 0; i < argc - 1; i++)
        for(int fl = 0; fl < ARGS; fl++)
            if(strcmp(flags[fl], argv[i]) == 0)
                *props[fl] = atoi(argv[i + 1]);
    create = MIN(create, MAX_PID - MIN_PID + 1);
    term = MIN(term, create);

    // create a bit-map
    allocate_map();

    // allocate some pids for processes created and display the result
    fprintf(stdout, " >>\e[4m main: %d processes seeking pid\e[0m\n\n", create);
    for(int i = 0; i < create; i++)
        fprintf(stdout, "allocated: %d\n", allocate_pid());
    display_map();

    // deallocate pids for those processes terminated and display the result
    fprintf(stdout, "\n >>\e[4m main: %d of them terminate\n\n\e[0m", term);
    for(int i = 0; i < term; i++)
        release_pid(i + MIN_PID);
    display_map();

    // let go of resources and exit
    release_map();
    return 0;
}