#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include "bitmap.h"

#define ON "\e[1;32m"
#define OFF  "\e[1;31m"

static map_t map;

int allocate_map(){
    int bytes;

    // set number of bits needed
    map.bits = MAX_PID - MIN_PID + 1;

    // allocate enough byte space to hold all bits (e.g 4 bytes for 25-32 bits)
    bytes = ceil((double)map.bits/CHAR_BIT);
    return (map.data = calloc(bytes, 1)) != NULL ? 1 : -1;
}

int allocate_pid(){
    int bytes, byte, bit;

    // create map if not exists
    if(map.data == NULL)
        allocate_map();

    // find the 1st byte with having at least 1 bit not set (unused pid)
    bytes = ceil((double)map.bits/CHAR_BIT);
    for(byte = 0; byte < bytes && map.data[byte] == UINT8_MAX; byte++);

    if(byte == bytes)   // all bits were set
        return -1;
    
    // find the 1st bit unset in that byte
    for(bit = 0; bit < CHAR_BIT && (map.data[byte] & (1 << bit)); bit++);

    // if this was the last byte, the bit might be invalid 
    // (e.g we need 10 bits so 2 bytes are allocate, bits indexed from 10 - 15 are invalid
    // while bits 0-8 are used)
    if(byte == bytes - 1 && bit > (MAX_PID - MIN_PID + 1) % CHAR_BIT)
        return -1;

    // set that bit and return
    map.data[byte] |= 1 << bit;
    return byte * CHAR_BIT + bit + MIN_PID;
}

int release_pid(int pid){
    int byte, bit, id = pid - MIN_PID;
    if(map.data == NULL || id > map.bits || id < 0)
        return -1;

    // find the exact bit and byte index for that pid and set it to 0
    byte = (double)id/CHAR_BIT;
    bit = id % CHAR_BIT;
    if((map.data[byte] & (1 << bit)) == 0) // if it is not taken, return -1;
        return -1;
    map.data[byte] &= ~(1 << bit);
    return 0;
}

// let go of resources allocated
void release_map(){
    map.bits = 0;
    free(map.data);
    map.data = NULL;
}

void display_map(){
    int bytes, bit, used;
    if(map.data == NULL)
        return;

    fprintf(stdout, "\e[1mBit-map holding ids in range [%d, %d]\n\e[0;1mPID\tUSED\e[0m\n", MIN_PID, MAX_PID);

    // for each pid(each bit of each byte), display the status
    bytes = ceil((double)map.bits/CHAR_BIT);
    for(int i = 0; i < bytes; i++){
        for(int j = 0; j < CHAR_BIT; j++){
            bit = i * CHAR_BIT + j;
            used = (map.data[i] & (1 << j)) > 0;

            if(bit == map.bits)
                fprintf(stdout, "\n\e[1m --- INTERNAL FRAGMENTATION (unused space) ---\e[0m\n\n");
            fprintf(stdout, "%d\t%s%hhu\e[0m\n", bit + MIN_PID, used ? ON : OFF, used);
        }
    }
}
