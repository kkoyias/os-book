#ifndef _bitmap_H
#define _bitmap_H

    #include <stdint.h>
    #define MIN_PID 30
    #define MAX_PID 40

    // bitmap definition
    typedef struct map{
        uint8_t *data;
        uint32_t bits;
    }map_t;

    // bitmap API
    int allocate_map();
    int allocate_pid();
    void release_map();
    int release_pid(int);

    void display_map();
#endif