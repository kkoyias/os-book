#!/bin/bash

# configurations
src=zombie.c
exe=$(echo $src | cut -d '.' -f 1)

# compile, run and keep track of the parent's pid
echo -n " >> compiling..."
gcc -o $exe $src
if [ $? -ne  0 ]
then
    echo -e "\e[1;31mFailed\e[0m to compile"
    exit 1
fi
echo -e "done\n >> running parent on the background"
./$exe &
pid=$!

# running ps to see zombie process
ps -l

# terminate parent process and exit
echo -n " >> killing parent process $pid..."
kill -9 $pid
echo "done"
exit 0