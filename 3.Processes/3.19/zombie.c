/*
 * forking a process that exits immediately, then sleep SECS seconds
 * so that it can get in zombie state for about SECS secs, use ps -l
 * to verify, or the script provided under this folder
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#define SECS 30

int main(){
    pid_t pid;

    if((pid = fork()) < 0){ // error - case
        perror("-- fork -- ");
        exit(EXIT_FAILURE);
    }

    // child
    else if(pid == 0) 
        exit(EXIT_SUCCESS);
        
    // parent
    else
        sleep(SECS);

    exit(EXIT_SUCCESS);
}