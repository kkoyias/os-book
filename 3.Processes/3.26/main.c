#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>

#define RD_END 0
#define WR_END 1
#define BUFF_SZ 100

void modify_msg(char *buf);
int main(){
    pid_t pid;
    size_t sz;
    char buff[BUFF_SZ];
    int send[2], recv[2];

    // create 2 pipes, one for the message to be forwarded
    // and 1 for the modified message to be received
    if(pipe(send) != 0 || pipe(recv) != 0){
        perror("-- pipe --");
        return 1;
    }

    if((pid = fork()) < 0){
        perror("-- fork --");
        return 2;
    }
    
    // -- parent --
    else if(pid > 0){

        // get message from stdin
        fprintf(stdout, " >> Parent: please insert message\n");
        fgets(buff, BUFF_SZ, stdin);

        // close ends not needed
        close(send[RD_END]);
        close(recv[WR_END]);

        // forward message and close this end, first send the length though
        fprintf(stdout, " >> Parent: sending message \"%s\" to child\n", buff);
        sz = strlen(buff);
        write(send[WR_END], &sz, sizeof(size_t));
        write(send[WR_END], buff, sz);
        close(send[WR_END]);

        // get modified message
        read(recv[RD_END], buff, strlen(buff));
        close(recv[RD_END]);

        fprintf(stdout, " >> Parent: message received --> \"%s\"\n", buff);
        return 0;
    }

    // -- child --
    close(recv[RD_END]);

    // get initial message, first get the length though
    read(send[RD_END], &sz, sizeof(size_t));
    read(send[RD_END], buff, sz);
    close(send[RD_END]); 


    // display initialize message and then modify it
    buff[sz] = '\0';
    fprintf(stdout, " >> Child: got %ld: \"%s\"\n", sz, buff);
    modify_msg(buff);

    // forward modified message
    write(recv[WR_END], buff, strlen(buff));
    close(recv[WR_END]);

    return 0;
}

// turn lower-case letters to upper-case and vice versa
void modify_msg(char *buf){
    assert(buf);

    for(int i = 0; i < strlen(buf); i++){

        if(buf[i] >= 'a' && buf[i] <= 'z') // lower
            buf[i] = 'A' + buf[i] - 'a';
        else if(buf[i] >= 'A' && buf[i] <= 'Z') // upper
            buf[i] = 'a' + buf[i] - 'A';
    }
}