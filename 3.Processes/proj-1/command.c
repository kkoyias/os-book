#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include "shell.h"

// make a command pointer point a newly created command or a previous one
int command_init(char *line, void *command){
    command_t **this = command;

    // split line into tokens to create command
    if(line[0] == PREV_COMMAND)
        *this = line[1] !=  PREV_COMMAND ? history_get(atoi(line + 1)) : history_latest();
    else
        command_new(line, command);
    return (*this != NULL) - 1; // return 0 on success, -1 on failure
}

// given a command pointer, make it point 
// to a command object created in the heap
int command_new(char *line, void *command){
    char delim[] = " ";
    command_t **this = command;
    assert(command);

    if((*this = malloc(sizeof(command_t))) == NULL)
        return -1;

    // count arguments, we need 2 more tokens(actual command and sentinel)
    strtok(line, delim);
    for((*this)->tokenc = 2; strtok(NULL, delim) != NULL; (*this)->tokenc++);

    // allocate enough space in the vector for all arguments
    if(((*this)->tokenv = malloc((*this)->tokenc * sizeof(token_t))) == NULL)
        return -1;

    // fill vector with arguments
    for(int i = 0; i < (*this)->tokenc - 1; i++){
        (*this)->tokenv[i] = malloc(strlen(line) + 1);
        strcpy((*this)->tokenv[i], line);
        line += strlen(line) + 1; // next argument
    }
    (*this)->tokenv[(*this)->tokenc - 1] = NULL; // sentinel

    return 0;
}

// execute a command replacing this process with another
void command_exec(void *command){
    command_t *this = command;
    assert(command);
    if(execvp(this->tokenv[0], this->tokenv) < 0){
        perror(this->tokenv[0]);
        exit(EXIT_FAILURE);
    }
}

// display a command token-by-token along with it's place in history
void command_print(void *command, int index){
    command_t *this = command;
    assert(command);

    if(this->tokenc > 0)
        fprintf(stdout, "%d \e[1m%s\e[0m ", index, this->tokenv[0]);
    for(int i = 1; i < this->tokenc - 2; i++)
        fprintf(stdout, "%s ", this->tokenv[i]);
    fprintf(stdout, "%s\n", this->tokenc > 2 ? this->tokenv[this->tokenc - 2] : "");
}

// let go of all space allocated for the arguments
void command_free(void *command, int index){
    command_t *this = command;
    assert(command);

    for(int i = 0; i < this->tokenc - 1; i++) // ignore SENTINEL
        free(this->tokenv[i]);
    free(this->tokenv);
    free(this);
}