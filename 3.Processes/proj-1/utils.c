#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <libgen.h>
#include <unistd.h>
#include "shell.h"

// set power to OFF once exit is invoked
int handle_exit(char **argv){
    extern int power;
    power = OFF;
    return 0;
}

int handle_history(char **argv){
    return history_print();
}

int handle_chdir(char **argv){
    assert(argv);
    return chdir(argv[1]);
}

// if the last character of $str is $ch, remove it and return 1 else return 0 
int rm_char(char *str, char ch){
    int rv, len;
    assert(str);

    if((rv = (str[(len = strlen(str)) - 1] == ch)))
        str[len - 1] = '\0';

    return rv;
}

// read a command from stdin with fgets, removing '\n' and '&' in order to run it
int read_command(char *line){
    assert(line);

    fgets(line, LINE - 1, stdin);
        
    // get rid of '\n'
    rm_char(line, '\n');
    return rm_char(line, '&');
}