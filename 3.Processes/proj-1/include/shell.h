#ifndef _SHELL_H
#define _SHELL_H

    #include "history.h"

    // definitions
    #define LINE 1024
    #define ON 1
    #define OFF 0
    #define SPECIALS 3
    #define LAST_COMMAND "!!"
    #define PREV_COMMAND '!'
    #define HISTORY_SIZE 10
    #define COLOR "\e[1;92m"
    #define FAIL_MARK "\e[1;7;31m\xE2\x98\xA0 \e[0m "

    // utilities
    int rm_char(char *, char);
    int read_command(char *);
    int handle_history(char **);
    int handle_exit(char **);
    int handle_chdir(char **);

#endif