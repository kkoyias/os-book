/*
 * a simple circular buffer implementation holding pointers to the actual
 * command objects to be freed on exit, there is a single static history
 * object pretty much like the singleton design pattern.
 */
#ifndef _HISTORY_H
#define _HISTORY_H

    #include "command.h"

    typedef struct history{
        int start;
        int end;
        int first;
        int length;
        command_t **commands;
    }history_t;

    // member methods
    int history_init(unsigned int);
    int history_insert(void *);
    int history_poll();
    void* history_latest();
    int history_isfull();
    int history_isempty();
    int history_foreach(void (*)(void*, int));
    int history_print();
    void *history_get(int);
    int history_size();
    int history_free();

#endif