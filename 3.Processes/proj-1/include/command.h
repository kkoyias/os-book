#ifndef _COMMAND_H
#define _COMMAND_H

    typedef char* token_t;
    typedef struct command{
        int tokenc;
        token_t* tokenv;
    }command_t;

    int command_init(char *, void *);
    int command_new(char *, void *);
    void command_exec(void *);
    void command_print(void *, int);
    void command_free(void *, int);

#endif