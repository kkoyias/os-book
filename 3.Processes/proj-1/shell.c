#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "shell.h"

int power = ON;
int main(){
    char line[LINE] = "\0", *home = getenv("HOME"), buff[LINE];
    command_t *cmd;
    char *commands[SPECIALS] = {"exit", "history", "cd"}; // special commands
    int (*handlers[SPECIALS])(char **) = {handle_exit, handle_history, handle_chdir}; // corresponding handlers
    int bg, status, i;
    pid_t pid;

    history_init(HISTORY_SIZE);
    for(power = ON; power != OFF; fflush(stdout)){

        // prompt user to enter a command
        getcwd(buff, LINE - 1);
        fprintf(stdout, COLOR"~%s\e[0m $ ", buff + strlen(home));
        fflush(stdout);

        // get command and determine whether to background it or not
        bg = read_command(line);

        if(strlen(line) == 0) // just ENTER pressed
            continue;

        else if(command_init(line, &cmd) == -1)
            continue;

        // handle special commands
        for(i = 0; i < SPECIALS; i++){
            if(strcmp(commands[i], cmd->tokenv[0]) == 0){
                handlers[i](cmd->tokenv);
                history_insert(cmd);
                break;
            }
        }
        if(i < SPECIALS)
            continue;

        // create a child process to execute the command
        if((pid = fork()) < 0){
            perror("-- fork --");
            exit(EXIT_FAILURE);
        }

        // -- child -- executes command
        else if(pid == 0)
            command_exec(cmd);

        // -- parent -- waits if command not to be backgrounded
        else if(!bg){
            wait(&status);
            if(WIFEXITED(status) && WEXITSTATUS(status) != EXIT_SUCCESS){
                fprintf(stdout, FAIL_MARK);
                continue;
            }
        }
        else
            fprintf(stdout, "[bg] %d\n", (int)pid); // backgrounded-process
        
        // insert command in history, omitting !* commands
        if(line[0] != PREV_COMMAND)
            history_insert(cmd);
    }

    history_free();
    exit(EXIT_SUCCESS);
}