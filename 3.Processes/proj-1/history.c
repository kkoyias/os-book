#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "shell.h"

static history_t history = {-1, 0, 1, 0, NULL};

int history_init(unsigned int length){
    if(history.commands != NULL)
        return -1;
    else if((history.commands = malloc(length * sizeof(command_t*))) == NULL)
        return -2;

    history.length = length;
    return 0;
}

int history_insert(void *command){
    if(history.commands == NULL)
        return -1;

    // remove the oldest command if history is full
    else if(history_isfull()){
        history.first++;
        history_poll();
    }
    else if(history_isempty())
        history.start = history.end = 0;
    
    history.commands[history.end] = command;
    history.end = (history.end + 1) % history.length;
    return 0;
}

int history_isempty(){
    if(history.commands == NULL)
        return -1;

    return history.start == -1;
}

int history_isfull(){
    if(history.commands == NULL)
        return -1;

    return history.start == history.end;
}

// remove the oldest command from history
int history_poll(){
    if(history.commands == NULL || history_isempty())
        return -1;

    command_free(history.commands[history.start], 0);
    if((history.start = (history.start + 1) % history.length) == history.end)
        history.start = -1;
    return 0;
}

// the last command is the one right before end pointer or the last one if end == 0
void* history_latest(){
    if(history.commands == NULL || history_isempty()){
        fprintf(stderr, FAIL_MARK "- history is empty\n");
        return NULL;
    }

    return history.commands[history.end ? history.end - 1 : history.length - 1];
}

// get the number of element the circular buffer holds
int history_size(){
    int size;
    if(history.commands == NULL || history_isempty())
        return 0;
    
    size = history.end - history.start;
    return size > 0 ? size : size + history.length;
}

// get the index-th command from history
void *history_get(int index){
    if(history.commands == NULL || history_isempty() || index < history.first || index > history.first + history_size() - 1){
        fprintf(stderr, FAIL_MARK "- no such event: %d\n", index);
        return NULL;
    }

    return history.commands[(index - history.first + history.start) % history.length];
}

int history_foreach(void (*callback)(void *, int)){
    assert(callback);
    if(history.commands == NULL)
        return -1;
    else if(history_isempty())
        return 1;

    callback(history.commands[history.start], history.first);
    for(int i = (history.start + 1) % history.length, j = 1; i != history.end; i = (i + 1) % history.length, j++)
        callback(history.commands[i], history.first + j);
    return 0;
}

int history_print(){
    return history_foreach(command_print);
}

int history_free(){

    history_foreach(command_free);
    free(history.commands); 
    history.commands = NULL;
    return 0;
}