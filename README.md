# os-book

This repo provides solutions to some of the
**coding problems** and **projects** of the famous book
["Operating Systems Concepts: 9th edition"](http://os-book.com/OS9/index.html)
by Avi Silberschatz, Peter Baer Galvin, Greg Gagne and more.
